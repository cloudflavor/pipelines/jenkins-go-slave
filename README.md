# jenkins-go-slave
ECS Jenkins slave for running go builds and tests.

For a lighter image: `docker build --build-arg GO_VERSION=1.9.2 -t jenkins-go-slave:${MY_TAG} --squash .`  

In this case `$GO_VERSION` needs to be on of the available versions on the
[golang download page](https://golang.org/dl/) and `$MY_TAG` is the desired tag
for the image, the current default go version is `1.9.2`.  
